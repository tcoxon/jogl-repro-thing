#version 120
uniform sampler2D texture0;
uniform vec2 texture0Size;
uniform ivec2 screenSize;
uniform ivec2 lcdSize;
uniform float aberration;
uniform vec2 glitchShift;
uniform vec2 vcrGlitchRange;
uniform float seed;

const vec2 crtCmpRadius = vec2(1.0, 0.75);
const mat3 cmpPos = mat3(
  0.33, 0.66, 0.0,
  0.50, 0.33, 0.0,
  0.66, 0.66, 0.0);
  
const float baseColorBlend = 0.3;
const float cmpBlend = 1.0;

const float barrelDistortion = 0.02;

float rand(vec2 co) {
  return fract(sin(seed + dot(co, vec2(12.9898, 78.233))) * 43758.5453);
}

vec3 readLcd(vec2 pos) {
  float f = (pos.x + pos.y * float(lcdSize.x)) / float(lcdSize.x * lcdSize.y);
  if (f >= vcrGlitchRange.x && f < vcrGlitchRange.y) {
    float gray = rand(floor(pos));
    return vec3(gray, gray, gray);
  }
  return texture2D(texture0, (pos / vec2(lcdSize)) * texture0Size).rgb;
}

vec3 sampleCrt(vec2 pos) {
  if (glitchShift.y < 0.0) {
    if (pos.y < -glitchShift.y) {
      pos.x += glitchShift.x;
    }
  } else {
    if (pos.y > glitchShift.y) {
      pos.x += glitchShift.x;
    }
  }
  pos *= vec2(lcdSize);
  vec2 fracPos = vec2(mod(pos.x, 1.0), mod(pos.y, 1.0));
  vec3 baseColor = readLcd(pos);
  vec3 samples = vec3(0.0, 0.0, 0.0);
  for (float dx = -1.0; dx < 2.0; dx += 1.0)
  for (float dy = -1.0; dy < 2.0; dy += 1.0) {
    vec2 d = vec2(dx,dy);
    if (aberration > 0.0) {
      vec3 rNeighbor = readLcd(pos + d + vec2(aberration, 0.0));
      vec3 gNeighbor = readLcd(pos + d);
      vec3 bNeighbor = readLcd(pos + d - vec2(aberration, 0.0));
      samples.r += rNeighbor.r * max(0.0, 1.0 - length((d + cmpPos[0].xy - fracPos) / crtCmpRadius));
      samples.g += gNeighbor.g * max(0.0, 1.0 - length((d + cmpPos[1].xy - fracPos) / crtCmpRadius));
      samples.b += bNeighbor.b * max(0.0, 1.0 - length((d + cmpPos[2].xy - fracPos) / crtCmpRadius));
    } else {
      vec3 neighbor = readLcd(pos + d);
      samples.r += neighbor.r * max(0.0, 1.0 - length((d + cmpPos[0].xy - fracPos) / crtCmpRadius));
      samples.g += neighbor.g * max(0.0, 1.0 - length((d + cmpPos[1].xy - fracPos) / crtCmpRadius));
      samples.b += neighbor.b * max(0.0, 1.0 - length((d + cmpPos[2].xy - fracPos) / crtCmpRadius));
    }
  }
  return baseColor * baseColorBlend + samples * cmpBlend;
}

vec3 sampleBarrel(vec2 pos) {
  pos -= vec2(0.5, 0.5);
  pos *= vec2(pow(length(pos), barrelDistortion));
  pos += vec2(0.5, 0.5);
  return sampleCrt(pos);
}

void main() {
  vec2 pos = gl_TexCoord[0].xy / texture0Size.xy;
  float brightness = min(1.0, (1.3 - length(pos - vec2(0.5, 0.5))));
  vec3 color = sampleBarrel(pos) * brightness;
  gl_FragColor = vec4(color, 1.0).rgba;
}
