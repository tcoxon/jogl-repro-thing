lazy val commonSettings = Seq(
  organization := "net.bytten",
  version := "0.0",
  scalaVersion := "2.10.3",
  scalaSource in Compile := { (baseDirectory in Compile)(_ / "src").value },
  classDirectory in Compile := { (baseDirectory in Compile)(_ / "bin").value },
  unmanagedBase := file("./lib"),
  autoCompilerPlugins := true,
  addCompilerPlugin("org.scala-lang.plugins" % "continuations" % "2.10.3"),
  scalacOptions += "-P:continuations:enable",
  scalacOptions += "-target:jvm-1.7",
  javacOptions ++= Seq("-source", "1.8", "-target", "1.8"),
  // Don't include scala version in output file paths:
  crossPaths := false,
  // Don't include our version number (0.0) in file paths:
  artifactName := { (_: ScalaVersion, _: ModuleID, artifact: Artifact) =>
    artifact.name + "." + artifact.extension
  }
)

lazy val repro = (project in file("repro")).
  settings(commonSettings:_*).
  settings(
    mainClass in Compile := None
  )

lazy val root = (project in file(".")).
  settings(commonSettings:_*).
  settings(
    mainClass in Compile := Some("repro.Main"),
    fork in run := true
  ).
  dependsOn(repro).
  aggregate(repro)
  
