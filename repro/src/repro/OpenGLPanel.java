package repro;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.geom.*;
import java.io.*;
import java.nio.*;

import com.jogamp.opengl.awt.*;
import com.jogamp.opengl.util.awt.*;
import com.jogamp.opengl.util.*;
import com.jogamp.opengl.util.texture.awt.*;
import com.jogamp.opengl.util.texture.*;
import com.jogamp.opengl.*;
import com.jogamp.common.nio.Buffers;

import javax.imageio.*;
import javax.imageio.plugins.jpeg.*;
import javax.imageio.stream.*;
import javax.swing.JFrame;

public class OpenGLPanel extends GamePanel implements GLEventListener {
    
    protected Texture texture;
    protected AnimatorBase animator;
    protected GLCanvas canvas;
    protected Overlay underlay;

    protected boolean shaderDirty;
    protected int shaderProgram;
    
    protected float chromaticAberration = 0.0f;
    protected Vec2D vcrGlitchRange = new Vec2D(0,0);
    protected boolean drawBackground = false;

    @Override
    protected void initGraphics() {
        System.out.println("Available GL profiles:");
        for (String profile: GLProfile.GL_PROFILE_LIST_ALL) {
            System.out.println(" - " + profile);
        }
        
        GLProfile glp;
        if (openGLProfileOverride == null) {
            glp = GLProfile.getGL2ES1();
        } else {
            glp = GLProfile.get(openGLProfileOverride);
        }
        System.out.println("Selected "+glp.getName());
        
        GLCapabilities caps = new GLCapabilities(glp);

        canvas = new GLCanvas(caps);
        canvas.setFocusable(false);
        canvas.addGLEventListener(this);
        canvas.addMouseListener(mouseListener);

        animator = new FPSAnimator(canvas, 60);
        add(canvas, BorderLayout.CENTER);

        animator.setUpdateFPSFrames(20, null);
        //animator.setRunAsFastAsPossible(true);
    }

    @Override
    public void init(GLAutoDrawable drawable) {
        GL gl = getGL(drawable);
        gl.setSwapInterval(0);
        underlay = new Overlay(drawable);

        resetShader(gl.getGL2());

        animator.start();
        requestFocus();
    }
    
    private GL getGL(GLAutoDrawable drawable) {
        GL gl = drawable.getGL();
        if (debugOpenGL && !hasDebugGL(gl)) {
            gl = wrapDebugGL(gl);
            drawable.setGL(gl);
        }
        if (traceOpenGL && !hasTraceGL(gl)) {
            gl = wrapTraceGL(gl);
            drawable.setGL(gl);
        }
        return gl;
    }
    
    private boolean hasDebugGL(GL gl) {
        if (gl instanceof DebugGL4bc) return true;
        if (gl.getDownstreamGL() != null) return hasDebugGL(gl.getDownstreamGL());
        return false;
    }
    
    private boolean hasTraceGL(GL gl) {
        if (gl instanceof TraceGL4bc) return true;
        if (gl.getDownstreamGL() != null) return hasTraceGL(gl.getDownstreamGL());
        return false;
    }
    
    private GL wrapDebugGL(GL gl) {
        if (gl.isGL4bc()) {
            System.out.println("Wrapping GL4bc with debugging");
            return new DebugGL4bc(gl.getGL4bc());
        } else if (gl.isGL4()) {
            System.out.println("Wrapping GL4 with debugging");
            return new DebugGL4(gl.getGL4());
        } else if (gl.isGL3bc()) {
            System.out.println("Wrapping GL3bc with debugging");
            return new DebugGL3bc(gl.getGL3bc());
        } else if (gl.isGL3()) {
            System.out.println("Wrapping GL3 with debugging");
            return new DebugGL3(gl.getGL3());
        } else if (gl.isGL2()) {
            System.out.println("Wrapping GL2 with debugging");
            return new DebugGL2(gl.getGL2());
        } else {
            System.out.println("Unable to wrap GL object with debugging: " + gl);
            return gl;
        }
    }
    
    private GL wrapTraceGL(GL gl) {
        if (gl.isGL4bc()) {
            System.out.println("Wrapping GL4bc with tracing");
            return new TraceGL4bc(gl.getGL4bc(), System.out);
        } else if (gl.isGL4()) {
            System.out.println("Wrapping GL4 with tracing");
            return new TraceGL4(gl.getGL4(), System.out);
        } else if (gl.isGL3bc()) {
            System.out.println("Wrapping GL3bc with tracing");
            return new TraceGL3bc(gl.getGL3bc(), System.out);
        } else if (gl.isGL3()) {
            System.out.println("Wrapping GL3 with tracing");
            return new TraceGL3(gl.getGL3(), System.out);
        } else if (gl.isGL2()) {
            System.out.println("Wrapping GL2 with tracing");
            return new TraceGL2(gl.getGL2(), System.out);
        } else {
            System.out.println("Unable to wrap GL object with tracing: " + gl);
            return gl;
        }
    }
    
    @Override
    protected void exiting() {
        animator.stop();
        super.exiting();
    }
    
    private int compileShader(GL2 gl, String source, String name, int type) {
        int shader = gl.glCreateShader(type);
        gl.glShaderSource(shader, 1, new String[]{source}, null,0);
        gl.glCompileShader(shader);
        
        IntBuffer intBuffer = IntBuffer.allocate(1);
        gl.glGetShaderiv(shader, GL2.GL_COMPILE_STATUS, intBuffer);
        if (intBuffer.get(0) != GL.GL_TRUE) {
            gl.glGetShaderiv(shader, GL2.GL_INFO_LOG_LENGTH, intBuffer);
            int size = intBuffer.get(0);
            System.err.println(name + " shader compile error (size "+size+"): ");
            if (size > 0) {
                ByteBuffer byteBuffer = ByteBuffer.allocate(size);
                gl.glGetShaderInfoLog(shader, size, intBuffer, byteBuffer);
                for (byte b: byteBuffer.array())
                    System.err.print((char) b);
                System.err.println();
            } else {
                System.err.println("Unknown");
            }
            return 0;
        }
        return shader;
    }
    
    private String readText(String path) {
        try (FileInputStream in = new FileInputStream(new File(path))) {
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            StringBuilder buf = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                buf.append(line);
                buf.append('\n');
            }
            return buf.toString();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void resetShader(GL2 gl) {
        shaderProgram = 0;

        String fragSource = readText("assets/screen.frag");
        int fragShader = compileShader(gl, fragSource, "Fragment", GL2.GL_FRAGMENT_SHADER);

        String vertSource = readText("assets/screen.vert");
        int vertShader = compileShader(gl, vertSource, "Vertex", GL2.GL_VERTEX_SHADER);

        shaderProgram = gl.glCreateProgram();
        if (fragShader != 0)
            gl.glAttachShader(shaderProgram, fragShader);
        if (vertShader != 0)
            gl.glAttachShader(shaderProgram, vertShader);
        gl.glLinkProgram(shaderProgram);

        IntBuffer intBuffer = IntBuffer.allocate(1);
        gl.glGetProgramiv(shaderProgram, GL2.GL_LINK_STATUS, intBuffer);
        if (intBuffer.get(0) != 1) {
            gl.glGetProgramiv(shaderProgram, GL2.GL_INFO_LOG_LENGTH, intBuffer);
            int size = intBuffer.get(0);
            System.err.println("Shader program link error (size "+size+"): ");
            if (size > 0) {
                ByteBuffer byteBuffer = ByteBuffer.allocate(size);
                gl.glGetProgramInfoLog(shaderProgram, size, intBuffer, byteBuffer);
                for (byte b: byteBuffer.array())
                    System.err.print((char) b);
                System.err.println();
            } else {
                System.err.println("Unknown");
            }

            shaderProgram = 0;
        }
    }

    @Override
    public void dispose(GLAutoDrawable drawable) {
        animator.stop();
    }
    
    private Vec2I getPotSize(Vec2I idealSize) {
        int w = 1;
        while (w < idealSize.x) w <<= 1;
        int h = 1;
        while (h < idealSize.y) h <<= 1;
        return new Vec2I(w, h);
    }
    
    int vboVertexHandle;
    int vboTexCoordHandle;
    FloatBuffer vertexData;
    FloatBuffer texCoordData;
    
    TextureCoords texCoords;
    Vec2I potSize;

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        GL2 gl = getGL(drawable).getGL2();

        bufferSize = new Vec2I(320, 176);
        potSize = getPotSize(bufferSize);
        //System.out.println("Allocating POT buffer " + potSize + " to fit texture " + bufferSize);
        buffer = createBufferedImage(potSize);
        bufferGfx = buffer.createGraphics();

        texture = AWTTextureIO.newTexture(drawable.getGLProfile(), buffer, false);
        gl.glActiveTexture(GL.GL_TEXTURE0);
        texture.bind(gl);
        texture.enable(gl);
        
        texCoords = texture.getSubImageTexCoords(0, potSize.y - bufferSize.y, bufferSize.x, potSize.y);

        vertexData = Buffers.newDirectFloatBuffer(4 * 3);
        vertexData.put(new float[]{-1,1,0});
        vertexData.put(new float[]{1,1,0});
        vertexData.put(new float[]{1,-1,0});
        vertexData.put(new float[]{-1,-1,0});
        vertexData.flip();
        
        texCoordData = Buffers.newDirectFloatBuffer(4 * 3);
        texCoordData.put(new float[]{texCoords.left(),texCoords.top()});
        texCoordData.put(new float[]{texCoords.right(),texCoords.top()});
        texCoordData.put(new float[]{texCoords.right(),texCoords.bottom()});
        texCoordData.put(new float[]{texCoords.left(),texCoords.bottom()});
        texCoordData.flip();
        
        IntBuffer buffers = IntBuffer.allocate(2);
        gl.glGenBuffers(2, buffers);
        vboVertexHandle = buffers.get(0);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboVertexHandle);
        gl.glBufferData(GL2.GL_ARRAY_BUFFER, 4 * 4 * 3, vertexData, GL2.GL_STATIC_DRAW);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, 0);
        
        vboTexCoordHandle = buffers.get(1);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboTexCoordHandle);
        gl.glBufferData(GL2.GL_ARRAY_BUFFER, 4 * 4 * 2, texCoordData, GL2.GL_STATIC_DRAW);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, 0);
    }

    @Override
    public void display(GLAutoDrawable drawable) {
        GL2 gl = getGL(drawable).getGL2();

        if (shaderDirty) {
            resetShader(gl);
            shaderDirty = false;
        }
        
        texture.enable(gl);
        gl.glEnableClientState(GL2.GL_VERTEX_ARRAY);
        gl.glEnableClientState(GL2.GL_TEXTURE_COORD_ARRAY);
        
        if (gl.hasGLSL() && useShader) {
            gl.glUseProgram(shaderProgram);
        }

        Graphics2D backG = underlay.createGraphics();
        paintGraphics();

        texture.updateImage(gl, AWTTextureIO.newTextureData(gl.getGLProfile(), buffer, false));
        texture.setTexParameteri(gl, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
        texture.setTexParameteri(gl, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
        gl.glActiveTexture(GL2.GL_TEXTURE0);
        texture.bind(gl);

        if (gl.hasGLSL() && useShader) {
            GL2 gl2 = gl.getGL2();
            int textureLoc = gl2.glGetUniformLocation(shaderProgram, "texture0");
            gl2.glUniform1i(textureLoc, 0);
            
            int textureSizeLoc = gl2.glGetUniformLocation(shaderProgram, "texture0Size");
            gl2.glUniform2f(textureSizeLoc, texCoords.right() - texCoords.left(), texCoords.bottom() - texCoords.top());
            
            int screenSizeLoc = gl2.glGetUniformLocation(shaderProgram, "screenSize");
            gl2.glUniform2i(screenSizeLoc, canvas.getWidth(), canvas.getHeight());
            int lcdSizeLoc = gl2.glGetUniformLocation(shaderProgram, "lcdSize");
            gl2.glUniform2i(lcdSizeLoc, bufferSize.x, bufferSize.y);
            int aberrationLoc = gl2.glGetUniformLocation(shaderProgram, "aberration");
            gl2.glUniform1f(aberrationLoc, chromaticAberration);
            int glitchShiftLoc = gl2.glGetUniformLocation(shaderProgram, "glitchShift");
            int vcrGlitchRangeLoc = gl2.glGetUniformLocation(shaderProgram, "vcrGlitchRange");
            gl2.glUniform2f(vcrGlitchRangeLoc, (float)vcrGlitchRange.x, (float)vcrGlitchRange.y);
            int seedLoc = gl2.glGetUniformLocation(shaderProgram, "seed");
            gl2.glUniform1f(seedLoc, (float)Math.random());
        }
        
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboVertexHandle);
        gl.glVertexPointer(3, GL2.GL_FLOAT, 0, 0l);
        
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboTexCoordHandle);
        gl.glTexCoordPointer(2, GL2.GL_FLOAT, 0, 0l);
        gl.glDrawArrays(GL2.GL_QUADS, 0, 4);
        
        if (gl.hasGLSL() && useShader)
            gl.glUseProgram(0);

        if (drawBackground) {
            underlay.markDirty(0, 0, getWidth(), getHeight());
            underlay.drawAll();
        }
        displayFPS();

        backG.dispose();

        gl.glFlush();
        
        gl.glDisableClientState(GL2.GL_TEXTURE_COORD_ARRAY);
        gl.glDisableClientState(GL2.GL_VERTEX_ARRAY);
        texture.disable(gl);

    }

    protected void afterReload() {
        shaderDirty = true;
    }

    @Override
    protected float getLastFPS() {
        return animator.getLastFPS();
    }
    
}
