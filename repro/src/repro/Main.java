package repro;

import java.util.Date;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.awt.*;
import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.lang.reflect.InvocationTargetException;

public class Main {
    
    public static void logSomeProperties() {
        String[] keys = new String[]{
                "sun.java2d.opengl",
                "sun.java2d.d3d",
                "sun.java2d.noddraw",
        };
        for (String key: keys) {
            System.out.println(key+"=" +
                System.getProperty(key, "(null)"));
        }
        
        System.out.println("isFullScreenSupported="+
                GraphicsEnvironment.getLocalGraphicsEnvironment().
                getDefaultScreenDevice().isFullScreenSupported());
        System.out.println();
    }
    
    public static void logVersion() {
        System.out.println(new Date());
        System.out.println("Class path: ");
        ClassLoader classLoader = Main.class.getClassLoader();
        if (classLoader instanceof URLClassLoader) {
            for (URL entry: ((URLClassLoader) classLoader).getURLs()) {
                System.out.println("    "+entry.toExternalForm());
            }
        } else {
            for (String entry: System.getProperty("java.class.path").split(File.pathSeparator)) {
                System.out.println("    "+entry);
            }
        }
        System.out.println();
        System.getProperties().list(System.out);
        System.out.println();
    }
    
    private static GamePanel createApplet() {
        return new OpenGLPanel();
    }
    
    public static void runInWindow(final String[] args) {
        final GameWindow window = new GameWindow("Repro");
        final GamePanel applet = createApplet();
        
        boolean[] fullscreen = new boolean[]{false};
        for (int i = 0; i < args.length; ++i) {
            String arg = args[i];
            if (arg.toLowerCase().equals("-fullscreen")) {
                fullscreen[0] = true;
            } else if (arg.toLowerCase().equals("-profile") && i + 1 < args.length) {
                applet.openGLProfileOverride = args[i+1];
                ++i;
            } else if (arg.toLowerCase().equals("-debugOpenGL")) {
                applet.debugOpenGL = true;
            } else if (arg.toLowerCase().equals("-traceOpenGL")) {
                applet.traceOpenGL = true;
            }
        }
        
        applet.setExitListener(new Runnable() {
            @Override
            public void run() {
                System.exit(0);
            }
        });
        applet.init();
        window.add(applet);
        window.validate();
        applet.start();
        applet.requestFocus();
        
        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    Frame frame = (Frame)window;
                    frame.addWindowListener(new WindowAdapter() {
                        @Override
                        public void windowClosing(WindowEvent event) {
                            System.out.println("closing");
                        }
                    });
                    frame.pack();
                    frame.setLocationRelativeTo(null);
                    frame.setVisible(true);
                }
            });
        } catch (InterruptedException|InvocationTargetException ex) {
            ex.printStackTrace();
        }
        
        applet.setParentFrame(window);
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                if (fullscreen[0]) {
                    applet.setFullscreen(true);
                }
            }
        });
                
    }
    
    public static void main(String[] args) {
        logVersion();
        logSomeProperties();
        for (String arg: args) {
            if (arg.toLowerCase().equals("-earlyinit")) {
                System.out.println("com.jogamp.opengl.GLProfile.initSingleton");
                com.jogamp.opengl.GLProfile.initSingleton();
                break;
            }
        }
        runInWindow(args);
    }
}
