package repro;

import javax.swing.*;
import java.io.*;
import java.awt.*;
import java.net.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class GameWindow extends JFrame {
    
    public GameWindow(String title) {
        super(title);
        
        setLayout(new BorderLayout());
        setResizable(false);
        
        Toolkit kit = Toolkit.getDefaultToolkit();
        try {
            Image img = kit.getImage(new File("assets/icon.png").toURI().toURL());
            setIconImage(img);
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        }
        
        setFocusTraversalKeysEnabled(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                
            }
        });
    }
}
