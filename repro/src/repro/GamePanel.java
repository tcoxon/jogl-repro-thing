package repro;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.geom.*;
import java.io.*;
import java.nio.*;
import javax.imageio.*;
import javax.swing.*;

public abstract class GamePanel extends JPanel {
    
    private static final long serialVersionUID = 1L;

    protected BufferedImage testImage;
    protected BufferedImage buffer;
    protected Vec2I bufferSize;
    protected Graphics2D bufferGfx;
    protected Rect2D texCoords;

    protected Runnable exitListener;

    protected Frame parentFrame;
    protected boolean fullScreen;
    protected boolean showFPS = true;
    protected boolean useShader = true;

    protected KeyListener keyListener;
    protected FocusListener focusListener;
    protected MouseListener mouseListener;
    
    protected String openGLProfileOverride = null;
    protected boolean debugOpenGL = false;
    protected boolean traceOpenGL = false;

    public GamePanel() {
        setPreferredSize(new Dimension(1280, 720));
        setFocusTraversalKeysEnabled(false);
        
        try {
            testImage = ImageIO.read(new File("assets/test-image.png"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void setParentFrame(Frame parentFrame) {
        this.parentFrame = parentFrame;
    }

    public void setExitListener(final Runnable exitListener) {
        this.exitListener = exitListener;
    }

    public Runnable getExitListener() {
        return exitListener;
    }

    public boolean isFullscreen() {
        return fullScreen;
    }

    private void hideCursor() {
        parentFrame.setCursor(parentFrame.getToolkit().createCustomCursor(
                new BufferedImage(1,1, BufferedImage.TYPE_INT_ARGB),
                new Point(), null));
    }

    private void showCursor() {
        parentFrame.setCursor(Cursor.getDefaultCursor());
    }
    
    public void setFullscreen(boolean fs) {
        if (parentFrame == null) return;
        final Window targetFullScreen;
        GraphicsDevice gd = parentFrame.getGraphicsConfiguration().getDevice();
        if (fs) {
            hideCursor();
        } else {
            showCursor();
        }
        if (!gd.isFullScreenSupported()) {
            System.out.println("Fullscreen is not supported on this platform.");
            fullScreen = fs;
            if (fs) {
                parentFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
            } else {
                parentFrame.setExtendedState(JFrame.NORMAL);
            }
            return;
        }
        if (fs) {
            parentFrame.dispose();
            parentFrame.setUndecorated(true);
            targetFullScreen = parentFrame;
            parentFrame.setResizable(false);
            parentFrame.setAlwaysOnTop(true);

        } else {
            parentFrame.dispose();
            parentFrame.setUndecorated(false);
            targetFullScreen = null;
            parentFrame.setResizable(false);
            parentFrame.setAlwaysOnTop(false);

            parentFrame.pack();
            parentFrame.setLocationRelativeTo(null);
        }
        gd.setFullScreenWindow(targetFullScreen);
        parentFrame.setVisible(true);
        requestFocus();
        fullScreen = fs;
        onFullscreenChange(fs);
    }
    
    protected void onFullscreenChange(boolean fs) {
    }

    public void init() {
        setFocusable(true);
        setBackground(Color.BLACK);
        setLayout(new BorderLayout());
        setSize(768, 576);

        focusListener = new FocusListener() {

            @Override
            public void focusGained(FocusEvent e) {
                System.out.println("focus gained");
            }

            @Override
            public void focusLost(FocusEvent e) {
                System.out.println("focus lost");
            }

        };
        this.addFocusListener(focusListener);

        keyListener = new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                switch (e.getKeyCode()) {
                case KeyEvent.VK_F:
                    if ((e.getModifiers() & InputEvent.CTRL_MASK) == 0) break;
                    // else fall through:
                case KeyEvent.VK_F11:
                    setFullscreen(!isFullscreen());
                    break;
                case KeyEvent.VK_F6:
                    useShader = !useShader;
                    break;
                case KeyEvent.VK_F5:
                    showFPS = !showFPS;
                    break;
                }
            }
        };
        addKeyListener(keyListener);

        mouseListener = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                onClick(e.getX(), e.getY());
                requestFocus();
            }
        };
        this.addMouseListener(mouseListener);

        initGraphics();

    }
    
    protected void exiting() {
        if (exitListener != null)
            exitListener.run();
    }

    public void start() {
    }

    protected abstract void initGraphics();

    protected Dimension bufferDim() {
        return new Dimension(bufferSize.x, bufferSize.y);
    }

    protected BufferedImage createBufferedImage(Vec2I size) {
        return new BufferedImage(size.x, size.y, BufferedImage.TYPE_INT_RGB);
    }

    public void paintGraphics() {
        bufferGfx.drawImage(testImage, null, 0, 0);
    }

    protected abstract float getLastFPS();

    public void displayFPS() {
        if (showFPS) {
            parentFrame.setTitle(String.format("%s (FPS: %.1f)", "Repro", getLastFPS()));
        } else {
            parentFrame.setTitle("Repro");
        }
    }

    protected void onClick(int x, int y) {
        int bx, by, w, h;

        int scale = Math.min(getWidth() / bufferSize.x,
                getHeight() / bufferSize.y);

        w = bufferSize.x * scale;
        h = bufferSize.y * scale;

        bx = (getWidth() - w) / 2;
        by = (getHeight() - h) / 2;

        Vec2I pos = new Vec2I((x-bx)/scale, (y-by)/scale);
        System.out.println("Clicked " + pos);
    }

}
