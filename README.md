Semi-minimal reproducer for issue affecting Lenna's Inception on linux devices with Mesa 20 and the iris driver.

Builds and runs with [SBT 1.3](https://www.scala-sbt.org/download.html):

`sbt run`

A few command line options are available:

* `sbt "run -fullscreen"` starts the app fullscreen. F11 also toggles fullscreen while it's running.
* `sbt "run -profile GL3bc"` selects a GL 3 compatibility profile. Without this, by default it'll look for GL2ES1.
* `sbt "run -debugOpenGL"` uses JOGL's debug wrapper
* `sbt "run -traceOpenGL"` uses JOGL's trace wrapper
* `sbt "run -earlyInit"` explicitly initializes JOGL's GLProfile singleton before creating any AWT/Swing windows

A prebuilt version is available here (which requires Java 8 to be installed): https://drive.google.com/open?id=1dY92f6TVe_PEYpqoiqZvzlV3X6gQ8qab
Use the included run.sh script to run it. The same command line options work, just drop the "sbt run" portion of the command.
